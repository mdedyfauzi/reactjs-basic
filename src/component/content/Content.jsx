import React from 'react';
import Card from '../card/Card';

class Content extends React.Component {
  constructor() {
    super();
    this.state = {
      nama: 'Agung',
      alamat: 'Jl. Astuti',
    };
  }

  changeName = () => {
    this.setState({ nama: 'Agung Hercules' });
  };

  changeAlamat = () => {
    this.setState({ alamat: 'Jl. Astuti and Barbel Melayang' });
  };

  render() {
    return (
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <Card nama={this.state.nama} alamat={this.state.alamat} />
        <button style={buttonStyle} onClick={this.changeName}>
          Change Name
        </button>
        <button style={buttonStyle} onClick={this.changeAlamat}>
          Change Address
        </button>
      </div>
    );
  }
}

const buttonStyle = {
  width: 120,
  marginTop: 5,
  marginLeft: 'auto',
  marginRight: 'auto',
  backgroundColor: 'white',
};

export default Content;
