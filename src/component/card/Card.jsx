import React from 'react';

const Card = ({ nama, alamat }) => {
  return (
    <>
      <h1 style={{ textAlign: 'center', backgroundColor: 'royalblue', color: 'gold', margin: 0 }}>nama : {nama}</h1>
      <h1 style={{ textAlign: 'center', backgroundColor: 'gold', color: 'royalblue', margin: 0 }}>alamat : {alamat}</h1>
    </>
  );
};

export default Card;
